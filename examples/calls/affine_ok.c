int f(int x) {
    int a = 2;
    int b = 4;
    return a*x + b;
}

void main() {
    int i = 2;
    i = f(f(i));
    assert(i == 20);
}
