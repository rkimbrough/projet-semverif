void f() {
    assert(false); //@KO
}

void main() {
    f();
    assert(false); //This assertion should not fail
}
