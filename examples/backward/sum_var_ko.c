void main() {
    int x = rand(1 , 10);
    int y = 3;
    int sum = x+y;
    if (sum >= 6)
    {
        sum = 0; // trap for the backward guard!
        // backward should find x in [3, 4]
        assert(x >= 5); //@KO
    }
}
