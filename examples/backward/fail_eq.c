void main() {
    int x = rand(1 , 10);
    int y = 3;
    int sum = x+y;
    if (sum == 6)
    {
        sum = 0; // trap for the backward guard!
        assert(x == 3); // should be ok, but impossible to prove with intervals :(
    }
}
