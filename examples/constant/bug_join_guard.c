void main() {
    int x = 1;
    int y = 1;
    /* Bug : join {x -> [], y -> 1} et {x -> 1, y -> []} */
    if(x < 0 || y < 0) {
        assert(false); //@OK
    }
}
