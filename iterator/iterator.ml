(*
  Cours "Sémantique et Application à la Vérification de programmes"

  Antoine Miné 2015
  Marc Chevalier 2018
  Josselin Giet 2021
  Ecole normale supérieure, Paris, France / CNRS / INRIA
*)

open Cfg

type etat =
  | EN_COURS
  | FINI

type direction =
  | FORWARD
  | BACKWARD

module MakeIterator(Dom : Domain.DOMAIN) = struct
  
  let to_widen = ref NodeSet.empty and etats = ref NodeMap.empty
  let rec dfs node =
    etats := NodeMap.add node EN_COURS !etats;
    let iter_arc arc =
      let nei = arc.arc_dst in
      match NodeMap.find_opt nei !etats with
      | Some EN_COURS -> to_widen := NodeSet.add nei !to_widen
      | Some FINI -> ()
      | None -> dfs nei in
    List.iter iter_arc node.node_out;
    etats := NodeMap.add node FINI !etats

  let remove_assert = ref false
  let rec iter_fct (envs, forward_envs) fct entry_node entry_dom mode =
    let get_env node =
      Option.value (NodeMap.find_opt node !envs) ~default:Dom.bottom
    and get_forward node =
      Option.value (NodeMap.find_opt node forward_envs) ~default:Dom.bottom
    and get_in_out node = match mode with
    | FORWARD -> node.node_in, node.node_out
    | BACKWARD -> node.node_out, node.node_in
    and get_src arc = match mode with
    | FORWARD -> arc.arc_src
    | BACKWARD -> arc.arc_dst
    and get_dst arc = match mode with
    | FORWARD -> arc.arc_dst
    | BACKWARD -> arc.arc_src in
    let entry = Option.value entry_node ~default:fct.func_entry in
    let entry_children = List.map get_dst (snd (get_in_out entry)) in
    let exit = match mode with
    | FORWARD -> fct.func_exit
    | BACKWARD -> fct.func_entry in
    (* Format.eprintf "fct %s, entry %d, dom %a@.\n" fct.func_name entry.node_id Dom.print entry_dom; *)

    (* On met [[les enfants]] de l'entrée pour éviter un bug. *)
    let worklist = ref (NodeSet.of_list entry_children) in
    envs := NodeMap.add entry entry_dom !envs;

    while (not (NodeSet.is_empty !worklist)) do
      let node = NodeSet.choose !worklist in
      (* Format.eprintf "[todel] look %d\n" node.node_id; *)
      worklist := NodeSet.remove node !worklist;
      let srcs, dests = get_in_out node in

      let apply_arc arc =
        let orig = (get_src arc) in
        let dom = get_env orig in
        (* Format.eprintf "%d -> %d\n" (get_src arc).node_id (get_dst arc).node_id; *)
        match arc.arc_inst with
        | CFG_skip _ -> dom
        | CFG_assign (var, expr) ->
          if mode = FORWARD then Dom.assign dom var expr
          else begin
            let ret, is_bot, s = Dom.bwd_assign (get_forward node) var expr dom in
            if is_bot then
              (remove_assert := true;
              Format.printf "Assignment at node %d: no correct value for %s\n" node.node_id var.var_name)
            else
              Format.printf "Assignment at node %d: %s can be %s\n" node.node_id var.var_name s;
            ret
          end
        | CFG_guard expr -> Dom.guard dom expr
        | CFG_assert (expr, ext) ->
          let true_branch = Dom.guard dom expr in
          let false_branch = Dom.guard dom (CFG_bool_unary (AST_NOT, expr)) in      
          if not (mode = BACKWARD || Dom.is_bottom false_branch) then begin
            remove_assert := false;
            if (!Options.backward) then begin
            Format.printf "%a@ " Errors.pp_err
            (AssertDetect, ext, expr);
            let failed_envs = ref (NodeMap.add orig false_branch NodeMap.empty) in
            ignore (iter_fct (failed_envs, !envs) fct (Some orig) false_branch BACKWARD)            
            end;
            let kind = if !remove_assert then Errors.AssertPass else Errors.AssertFalse in
            Format.printf "%a@ " Errors.pp_err
              (kind, ext, expr);
          end;
          true_branch
        | CFG_call fct -> if (!Options.backward) then failwith "calls not supported with backward analysis";
          iter_fct (envs, forward_envs) fct None dom FORWARD in

      let alive_srcs = List.filter (fun arc -> NodeMap.mem (get_src arc) !envs) srcs in
      let srcs_env = List.map apply_arc alive_srcs in
      let new_env = List.fold_left Dom.join Dom.bottom srcs_env in
      if (not (NodeMap.mem node !envs)) && (new_env <> (get_env node)) then begin
        envs := NodeMap.add node (if NodeSet.mem node !to_widen then Dom.widen (get_env node) new_env else new_env) !envs;
        let adder arc =
          worklist := NodeSet.add (get_dst arc) !worklist in
        List.iter adder dests
      end;
      (* Format.eprintf "node %d : %a@." node.node_id Dom.print new_env *)
    done;
    (* Retourne le domaine de la sortie de la fonction *)
    let exit_dom = (NodeMap.find_opt exit !envs) in
    Option.value exit_dom ~default:Dom.bottom

  let iterate cfg =
    let () = Random.self_init () in
    (* Lance DFS depuis l'entrée de chaque fct *)
    List.iter dfs (List.map (fun f -> f.func_entry) cfg.cfg_funcs);
    NodeSet.iter (fun node -> Format.eprintf "%d marked\n" node.node_id) !to_widen;

    let envs = ref (NodeMap.empty) in
    let main_fct = List.find (fun f -> f.func_name = "main") cfg.cfg_funcs in
    let _ = iter_fct (envs, NodeMap.empty) main_fct None Dom.init FORWARD in ()
end
