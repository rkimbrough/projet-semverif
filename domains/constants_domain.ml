open Domain
open Abstract_syntax_tree
open Value_domain
open Cfg

let is_null = Z.equal Z.zero

module CstValueDomain : VALUE_DOMAIN = struct
  type t =
  | CstBottom
  | CstTop
  | CstConst of Z.t
  let top = CstTop
  let bottom = CstBottom
  let const c = CstConst c
  let rand a b = if Z.equal a b then CstConst a else top
  
  let join x y = match x, y with
  | CstBottom, _ -> y
  | _, CstBottom -> x
  | CstConst a, CstConst b when Z.equal a b -> x
  | _, _ -> CstTop

  let meet x y = match x, y with
  | CstTop, _ -> y
  | _, CstTop -> x
  | CstConst a, CstConst b when Z.equal a b -> x
  | _, _ -> CstBottom

  let unary x = function
  | AST_UNARY_PLUS -> x
  | AST_UNARY_MINUS -> begin
    match x with
    | CstBottom | CstTop -> x
    | CstConst c -> CstConst (Z.neg c) 
  end

  let binary x y =
    let discriminate op if_right_is_zero if_left_is_zero = match x, y with
    | CstBottom, _ | _, CstBottom -> CstBottom
    | _, CstConst z when is_null z -> if_right_is_zero
    | CstConst z, _ when is_null z -> if_left_is_zero
    | CstTop, _ | _, CstTop -> CstTop
    | CstConst a, CstConst b -> CstConst (op a b)
    in
    let cstzero = CstConst Z.zero in
  function
  | AST_PLUS -> discriminate Z.add x y
  | AST_MINUS -> discriminate Z.sub x (unary y AST_UNARY_MINUS)
  | AST_MULTIPLY -> discriminate Z.mul cstzero cstzero
  | AST_DIVIDE -> discriminate Z.div CstBottom cstzero
  | AST_MODULO -> discriminate Z.rem CstBottom cstzero
 
  let compare x y =
    let discriminate neg_cmp = match x, y with
    | CstBottom, _ | _, CstBottom -> (CstBottom, CstBottom)
    | CstConst a, CstConst b when neg_cmp a b -> (CstBottom, CstBottom)
    | _, _ -> (x, y)
    in
  function
  | AST_EQUAL -> begin
    match x, y with
    | CstBottom, _ | _, CstBottom -> (CstBottom, CstBottom)
    | CstConst a, CstConst b when not (Z.equal a b) -> (CstBottom, CstBottom)
    | CstConst _, _ -> (x, x)
    | _, CstConst _ -> (y, y)
    | CstTop, CstTop -> (CstTop, CstTop)
  end
  | AST_NOT_EQUAL -> discriminate Z.equal
  | AST_LESS -> discriminate Z.geq
  | AST_LESS_EQUAL -> discriminate Z.gt
  | AST_GREATER -> discriminate Z.leq
  | AST_GREATER_EQUAL -> discriminate Z.lt

  let bwd_unary x op r = meet x (unary r op)

  let bwd_binary x y op r =
    let meet_binary x y rev_op = meet (binary x y rev_op) in
    match op with
    | AST_PLUS -> (meet_binary r y AST_MINUS x, meet_binary r x AST_MINUS y)
    | AST_MINUS -> (meet_binary y r AST_PLUS x, meet_binary x r AST_MINUS y)
    | AST_MULTIPLY ->
      (* On veut savoir les valeurs de x possibles si x * y = r *)
      let rev_mul x y r = match y, r with
      | CstConst z, CstConst z' when is_null z && is_null z' -> x
      | CstConst z, CstConst _ when is_null z -> CstBottom
      | CstConst b, CstConst c when not (is_null (Z.rem c b)) -> CstBottom
      | _, _ -> meet_binary r y AST_DIVIDE x
      in
      (rev_mul x y r, rev_mul y x r)
    | AST_DIVIDE | AST_MODULO -> (x, y)

  let subset x y = match x, y with
  | CstBottom, _ -> true
  | CstConst a, CstConst b when Z.equal a b -> true
  | _, CstTop -> true
  | _, _ -> false
  let is_bottom = function CstBottom -> true | _ -> false

  let widen = join
  let narrow x y = if subset y x then y else CstBottom

  let print f = function
  | CstBottom -> Format.fprintf f "{}"
  | CstTop -> Format.fprintf f "Z"
  | CstConst c -> Format.fprintf f "%d" (Z.to_int c)
end

module CstDomain(Vars : VARS) = Make_domain.MakeDomain(CstValueDomain)(Vars)
