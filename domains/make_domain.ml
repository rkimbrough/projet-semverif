open Domain
open Value_domain
open Cfg
open Abstract_syntax_tree

let neg_compare_op = function
| AST_EQUAL -> AST_NOT_EQUAL
| AST_NOT_EQUAL -> AST_EQUAL
| AST_LESS -> AST_GREATER_EQUAL
| AST_LESS_EQUAL -> AST_GREATER
| AST_GREATER -> AST_LESS_EQUAL
| AST_GREATER_EQUAL -> AST_LESS

module MakeDomain(VDom : VALUE_DOMAIN)(Vars : VARS) : DOMAIN = struct
  type t = VDom.t VarMap.t

  type int_note_ast =
  | NOTE_int_unary of int_unary_op * int_note
  | NOTE_int_binary of int_binary_op * int_note * int_note
  | NOTE_int_var of var
  | NOTE_int_const of Z.t
  | NOTE_int_rand of Z.t (* lower bound *) * Z.t (* upper bound *)
  and int_note = int_note_ast * VDom.t

  (** Annote l'arbre d'expression avec le VDom de chaque noeud *)
  let rec annotate_ast dom expr =
    let aux = annotate_ast dom in
    match expr with
    | CFG_int_unary (op, e) ->
      let e' = aux e in
      (NOTE_int_unary (op, e')), VDom.unary (snd e') op

    | CFG_int_binary (op, e1, e2) ->
      let e1' = aux e1 and e2' = aux e2 in
      (NOTE_int_binary (op, e1', e2')), VDom.binary (snd e1') (snd e2') op
    
    | CFG_int_var v -> NOTE_int_var v, VarMap.find v dom
    | CFG_int_const c -> NOTE_int_const c, VDom.const c
    | CFG_int_rand (a, b) -> NOTE_int_rand (a, b), VDom.rand a b

  let init =
    let make_default v = (v, VDom.const (Z.of_int 0)) in
    VarMap.of_list (List.map make_default Vars.support)

  let bottom =
    let make_bottom v = (v, VDom.bottom) in
    VarMap.of_list (List.map make_bottom Vars.support)

  let assign dom var expr =
    let _, vdRoot = annotate_ast dom expr in
    VarMap.add var vdRoot dom

  (** Applique op sur chaque VDom.
      map2zo : il faut que op x x = x. *)
  let pointwise op x y =
    VarMap.map2z (fun _ -> op) x y

  let join = pointwise VDom.join
  let meet = pointwise VDom.meet
  let widen = pointwise VDom.widen
  let narrow = pointwise VDom.narrow

  (** Propage le nouveau vdom connu pour le noeud actuel vers
      ses enfants, met à jour le domaine qd c'est une var. *)
  let rec propage dom_ref node newVd = match (fst node) with
  | NOTE_int_unary (op, e) -> propage dom_ref e (VDom.bwd_unary (snd e) op newVd)
  | NOTE_int_binary (op, e1, e2) ->
    let v1, v2 = VDom.bwd_binary (snd e1) (snd e2) op newVd in
    propage dom_ref e1 v1; propage dom_ref e2 v2
  | NOTE_int_var v -> dom_ref := VarMap.add v newVd !dom_ref
  | _ -> () (* const/rand ne fait rien *)
    
  let bwd_assign x var expr r =
    (* Sauvegarde les valeurs prises après assignement *)
    let vd_var = VarMap.find var r in
    let svar = Format.asprintf "%a" VDom.print vd_var in
    (* Propage le VDom connu sur var *)
    let ast = annotate_ast x expr and dom_ref = ref x in
    propage dom_ref ast (VarMap.find var r);
    dom_ref := meet !dom_ref r;
    (* On ne connait plus la variable avant assignement *)
    dom_ref := VarMap.add var VDom.top !dom_ref;
    !dom_ref, VDom.is_bottom vd_var, svar

  let guard_compare dom op e1 e2 =
    let ast1 = annotate_ast dom e1 and ast2 = annotate_ast dom e2 in
    (** Nouveaux vdom issus de compare pour les racines. *)
    let dr1, dr2 = VDom.compare (snd ast1) (snd ast2) op in
    let dom_ref = ref dom in
    propage dom_ref ast1 dr1; propage dom_ref ast2 dr2; !dom_ref

  (** Existe-t-il un VDom vide ? *)
  let is_bottom = VarMap.exists (fun _ -> VDom.is_bottom)
  (** Renvoie bottom si is_bottom = true *)
  let normalize dom = if (is_bottom dom) then bottom else dom
  let guard dom =
    (* Propage les négations vers les CFG_compare *)
    let rec aux (flag : bool) expr = normalize (match expr with
    | CFG_bool_unary (AST_NOT, e) -> aux (not flag) e
    | CFG_bool_binary (op, e1, e2) ->
      (* Lois de De Morgan dans le choix de f *)
      let f = if ((op = AST_AND) <> flag) then meet else join in
      f (aux flag e1) (aux flag e2)
    | CFG_compare (op, e1, e2) ->
      guard_compare dom (if flag then (neg_compare_op op) else op) e1 e2
    | CFG_bool_const c -> if (c <> flag) then dom else bottom
    | CFG_bool_rand -> dom) in
    aux false

  (** Pointwise inclusion *)
  let subset = VarMap.for_all2z (fun _ -> VDom.subset)
  let print f dom =
    Format.fprintf f "{";
    VarMap.iter (fun var vd -> Format.fprintf f "%s -> " var.var_name; VDom.print f vd; Format.fprintf f "; ") dom;
    Format.fprintf f "}"
end
