open Domain
open Abstract_syntax_tree
open Value_domain
open Cfg

let z_is_null = Z.equal Z.zero

module Zinf = struct
  type t =
  | MinusInf
  | Finite of Z.t
  | PlusInf
  let zero = Finite Z.zero
  let one = Finite Z.one

  let eq x y = match x, y with
  | MinusInf, MinusInf | PlusInf, PlusInf -> true
  | Finite a, Finite b when Z.equal a b -> true
  | _, _ -> false
  let neq x y = not (eq x y)
  let leq x y = match x, y with
  | MinusInf, _ | _, PlusInf -> true
  | Finite a, Finite b when Z.leq a b -> true
  | _, _ -> false
  let lt x y = leq x y && neq x y
  let geq x y = not (lt x y)
  let gt x y = not (leq x y)

  let ispos = function
  | MinusInf -> false
  | Finite a -> Z.lt Z.zero a
  | PlusInf -> true
  let isneg = function
  | MinusInf -> true
  | Finite a -> Z.lt a Z.zero
  | PlusInf -> false
  let isnull = function
  | Finite z when z_is_null z -> true
  | _ -> false
  let isinf = function
  | MinusInf | PlusInf -> true
  | _ -> false

  let min x y = if leq x y then x else y
  let max x y = if leq x y then y else x

  let neg = function
  | MinusInf -> PlusInf
  | Finite a -> Finite (Z.neg a)
  | PlusInf -> MinusInf

  let abs x = if isneg x then neg x else x

  let add x y = match x, y with
  | MinusInf, PlusInf | PlusInf, MinusInf -> failwith "undefined operation result"
  | MinusInf, _ | _, MinusInf -> MinusInf
  | PlusInf, _ | _, PlusInf -> PlusInf
  | Finite a, Finite b -> Finite (Z.add a b)

  let sub x y = add x (neg y)

  let mul x y =
    let mul_abs x y = match x, y with
    | Finite z, _ when z_is_null z -> zero
    | _, Finite z when z_is_null z -> zero
    | PlusInf, _ | _, PlusInf -> PlusInf
    | Finite a, Finite b -> Finite (Z.mul a b)
    | _, _ -> failwith "negative absolute values"
    in

    let res_abs = mul_abs (abs x) (abs y) in
    let res_neg = isneg x <> isneg y in
    if res_neg then neg res_abs else res_abs
  
  let div x y = match x, y with
    | _, MinusInf | _, PlusInf -> zero
    | _, Finite z when z_is_null z -> failwith "division by zero"
    | MinusInf, _ -> if ispos y then MinusInf else PlusInf
    | PlusInf, _ -> if ispos y then PlusInf else MinusInf
    | Finite a, Finite b -> Finite (Z.div a b)

  let print f = function
  | MinusInf -> Format.fprintf f "-oo"
  | Finite a -> Format.fprintf f "%d" (Z.to_int a)
  | PlusInf -> Format.fprintf f "oo"
end

module IntValueDomain : VALUE_DOMAIN = struct
  type t =
  | Empty
  | Inter of Zinf.t * Zinf.t
  let normalize = function
  | Empty -> Empty
  | Inter (x, y) ->
    if Zinf.eq x Zinf.PlusInf || Zinf.eq y Zinf.MinusInf || Zinf.gt x y then Empty
    else Inter (x, y)

  let top = Inter (Zinf.MinusInf, Zinf.PlusInf)
  let bottom = Empty
  let const c = Inter (Zinf.Finite c, Zinf.Finite c)
  let rand a b = Inter (Zinf.Finite (Z.min a b), Zinf.Finite (Z.max a b))

  let join u v = match u, v with
  | Empty, _ -> v
  | _, Empty -> u
  | Inter (xu, yu), Inter (xv, yv) -> Inter (Zinf.min xu xv, Zinf.max yu yv)
  let meet u v = match u, v with
  | Empty, _ | _, Empty -> Empty
  | Inter (xu, yu), Inter (xv, yv) -> normalize (Inter (Zinf.max xu xv, Zinf.min yu yv))
  
  let unary u = function
  | AST_UNARY_PLUS -> u
  | AST_UNARY_MINUS -> begin
    match u with
    | Empty -> Empty
    | Inter (x, y) -> Inter (Zinf.neg y, Zinf.neg x)
  end

  let binary =
    let rec aux u v op = match u, v with
    | Empty, _ | _, Empty -> Empty
    | Inter (xu, yu), Inter (xv, yv) -> begin
      match op with
      | AST_PLUS -> Inter (Zinf.add xu xv, Zinf.add yu yv)
      | AST_MINUS -> Inter (Zinf.sub xu yv, Zinf.sub yu xv)
      | AST_MULTIPLY ->
        let min4 n1 n2 n3 n4 = Zinf.min (Zinf.min n1 n2) (Zinf.min n3 n4)
        and max4 n1 n2 n3 n4 = Zinf.max (Zinf.max n1 n2) (Zinf.max n3 n4)
        in
        let xuxv = Zinf.mul xu xv
        and xuyv = Zinf.mul xu yv
        and yuxv = Zinf.mul yu xv
        and yuyv = Zinf.mul yu yv
        in
        Inter (min4 xuxv xuyv yuxv yuyv, max4 xuxv xuyv yuxv yuyv)
      | AST_DIVIDE ->
        let div_pos u v = match u, v with
        | Empty, _ | _, Empty -> Empty
        | Inter (xu, yu), Inter (xv, yv) -> Inter (Zinf.div xu yv, Zinf.div yu xv)
        in
        let upos = normalize (Inter (Zinf.max Zinf.zero xu, yu))
        and uneg = normalize (Inter (Zinf.max Zinf.one (Zinf.neg yu), (Zinf.neg xu)))
        and vpos = normalize (Inter (Zinf.max Zinf.one xv, yv))
        and vneg = normalize (Inter (Zinf.max Zinf.one (Zinf.neg yv), (Zinf.neg xv)))
        in
        join (join (div_pos upos vpos) (div_pos uneg vneg)) (unary (join (div_pos uneg vpos) (div_pos upos vneg)) AST_UNARY_MINUS)
      | AST_MODULO -> aux u (aux v (aux u v AST_DIVIDE) AST_MULTIPLY) AST_MINUS
      end
    in aux

  let cp_safe l1 r1 l2 r2 op = try
    let lu, ru = match op with
    | AST_EQUAL -> Zinf.max l1 l2, Zinf.min r1 r2
    | AST_NOT_EQUAL ->
      if Zinf.eq l2 r2 && Zinf.eq r1 l2 then
        l1, Zinf.sub r1 Zinf.one
      else if Zinf.eq l2 r2 && Zinf.eq r2 l1 then
        Zinf.add l1 Zinf.one, r1
      else
        l1, r1
    | AST_LESS -> l1, Zinf.min r1 (Zinf.sub r2 Zinf.one)
    | AST_LESS_EQUAL -> l1, Zinf.min r1 r2
    | AST_GREATER -> Zinf.max l1 (Zinf.add l2 Zinf.one), r1
    | AST_GREATER_EQUAL -> Zinf.max l1 l2, r1
    in
    normalize (Inter (lu, ru))
  with Assert_failure _ -> Empty
  
  let compare u v op =
    let rev_compare_op = function
    | AST_EQUAL -> AST_EQUAL
    | AST_NOT_EQUAL -> AST_NOT_EQUAL
    | AST_LESS -> AST_GREATER
    | AST_LESS_EQUAL -> AST_GREATER_EQUAL
    | AST_GREATER -> AST_LESS
    | AST_GREATER_EQUAL -> AST_LESS_EQUAL
    in
    match (u, v) with
    | Inter (l1, r1), Inter (l2, r2) ->
      cp_safe l1 r1 l2 r2 op, cp_safe l2 r2 l1 r1 (rev_compare_op op)
    | _ -> Empty, Empty
  

  let bwd_unary u op r = meet u (unary r op)

  let bwd_binary u v op r =
    let meet_binary u v rev_op = meet (binary u v rev_op) in
    match op with
    | AST_PLUS -> (meet_binary r v AST_MINUS u, meet_binary r u AST_MINUS v)
    | AST_MINUS -> (meet_binary v r AST_PLUS u, meet_binary u r AST_MINUS v)
    | AST_MULTIPLY -> begin
      match u, v, r with
      | Empty, _, _ | _, Empty, _ | _, _, Empty -> Empty, Empty
      | Inter (xu, yu), Inter (xv, yv), Inter (xr, yr) ->
        let div_st_pos_round u v = match u, v with
        | Empty, _ | _, Empty -> Empty
        | Inter (xu, yu), Inter (xv, yv) ->
          (*
            On veut arrondir au supérieur la borne inf pour ne pas inclure de faux candidats.
            On suppose que xu > 0 (et on traite le cas xu = 0 à la main avant d'appeler la fonction).
          *)
          if Zinf.eq yv Zinf.PlusInf then Inter (Zinf.one, Zinf.div yu xv)
          else Inter (Zinf.div (Zinf.add xu (Zinf.sub yv Zinf.one)) yv, Zinf.div yu xv)
        in

        let has_zero x y = Zinf.leq x Zinf.zero && Zinf.leq Zinf.zero y in

        let rpos = normalize (Inter (Zinf.max Zinf.one xr, yr))
        and rneg = normalize (Inter (Zinf.max Zinf.one (Zinf.neg yr), Zinf.neg xr))
        in

        (* Si ? * [x, y] = [xr, yr], on veut savoir les valeurs prises par ? *)
        let reverse x y =
          (* Si 0 est dans les deux intervalles, n'importe quelle valeur peut aller dans ? *)
          if has_zero x y && has_zero xr yr then
            top
          (* Sinon, on fait juste une division *)
          else
            let pos = normalize (Inter (Zinf.max Zinf.one x, y))
            and neg = normalize (Inter (Zinf.max Zinf.one (Zinf.neg y), Zinf.neg y))
            in
            join
              (join (div_st_pos_round rpos pos) (div_st_pos_round rneg neg))
              (unary (join (div_st_pos_round rneg pos) (div_st_pos_round rpos neg)) AST_UNARY_MINUS)
        in
        (
          meet (reverse xv yv) u,
          meet (reverse xu yu) v
        )
    end
    | _ -> (u, v)

  let subset u v = match u, v with
  | Empty, _ -> true
  | _, Empty -> false
  | Inter (xu, yu), Inter (xv, yv) -> Zinf.leq xv xu && Zinf.leq yu yv

  let is_bottom u = (u = Empty)
  
  let widen u v = match u, v with
  | Empty, _ -> v
  | _, Empty -> u
  | Inter (xu, yu), Inter (xv, yv) ->
    let x = if Zinf.leq xu xv then xu else Zinf.MinusInf
    and y = if Zinf.leq yv yu then yu else Zinf.PlusInf
    in
    Inter (x, y)

  let narrow u v = match u, v with
  | Empty, _ -> v
  | _, Empty -> u
  | Inter (xu, yu), Inter (xv, yv) ->
    let x = if Zinf.eq xu Zinf.MinusInf then xv else xu
    and y = if Zinf.eq yu Zinf.PlusInf then yv else yu
    in
    Inter (x, y)

  let print f = function
  | Empty -> Format.fprintf f "{}"
  | Inter (u, v) -> Format.fprintf f "[%a, %a]" Zinf.print u Zinf.print v
end

module IntDomain(Vars : VARS) = Make_domain.MakeDomain(IntValueDomain)(Vars)
