# Analyseur statique pour un simili-C

Projet du cours de sémantique et vérification formelle de Hugo Peyraud-Magnin et Rémy Kimbrough.

## Résultats des tests
```
test: 78 (files: 45)
Soudness      OK 
Completness   KO (13)
```

## Itérateur

L'itérateur implémente un algorithme de worklist classique. Avant son lancement, on fait un parcours en profondeur depuis chaque entrée de fonction pour trouver les cycles. On marque alors au moins un noeud par cycle (tous les voisins déjà en cours d'exploration). Généralement, ce sont les têtes de boucles qui sont marquées.

Lorsqu'un noeud est marqué, son nouvel environnement est `ancien_environnement widen (join des arcs entrants)`.

## Domaines
Nous avons implémenté le domaine des constantes et celui des intervalles. Voici quelques unes des solutions que nous avons trouvées pour simplifier le code et/ou résoudre certains problèmes.

### Domaine des constantes
`binary` : Pour les opérations binaires, les cas remarquables sont, dans l'ordre décroissant de priorité :
- lorsqu'un des deux opérandes est Bottom (le résultat est aussi Bottom)
- puis lorsqu'un des deux opérandes est `0` (pour la multiplication, la division et le modulo)
- puis lorsqu'un des deux opérandes est Top (le résultat est aussi Top)
- enfin, lorsque les deux sont des constantes non nulles (on peut simplement calculer le résultat de l'opération)

Afin de factoriser le code, nous avons donc écrit une fonction auxiliaire `discriminate` qui prend aussi le résultat à fournir lorsque l'opérande de droite (resp. de gauche) est nul.

`compare` : L'égalité est traitée à part. On peut factoriser le code des autres comparaisons en remarquant que soit le résultat est vide (lorsqu'un des comparés est vide ou que ce sont des constantes qui ne vérifient pas la comparaison), soit que la comparaison ne filtre pas.

`bwd_unary` : Les opérations unaires sont involutives, on peut donc intersecter `x` avec l'antécédent (facilement calculable) de `r`.

`bwd_binary` : On sait facilement inverser `+` et `-`. Pour `*`, c'est plus compliqué car il faut traîter le cas où un des deux opérandes est `0`, et la division entière n'est pas réellement l'opération inverse, mais on s'en sort. 

### Domaine des intervalles
`module Zinf` : Il a fallu coder un module étendant `Z` avec les infinis. Le code est sans surprise.

`normalize` : Une fonction utilitaire permettant de ramener tous les intervalles virtuellement vides à `Empty`. Utile pour calculer des intersections.

`binary` : La multiplication, la division et le modulo sont délicats. On utilise les solutions abordées en TD.

`compare` : C'est direct, à l'exception de `!=`, qui doit être traitée plus intelligemment lorsqu'un des deux comparés est un singleton. On le retire à l'autre intervalle, ce qui ne change rien **sauf** lorsqu'il s'agissait d'une des bornes, auquel cas il faut la retirer.

`bwd_unary` : Comme pour les constantes.

`bwd_binary` : Ici aussi, c'est délicat pour la multiplication. On commence par traiter à part un cas où `0` apparaît dans une opérande, puis on fait une division un peu différente où on arrondit la borne inférieure **à l'entier supérieur** pour ne pas inclure de faux antécédants.

### Foncteur `Make_domain`
Nous avons factorisé la création d'un domaine à partir du domaine des valeurs. Les opérations sont faites point par point. Le seul point délicat est `guard` :

Pour une comparaison, on annote les arbres des expressions avec une fonction qui calcule (de bas en haut) pour chaque noeud de l'AST son domaine. On applique ensuite `VDom.compare` sur les racines des deux arbres, puis on les propage (de haut en bas) dans les arbres annotés.

Pour `&&` et `||`, on utilise `meet` et `join`. La négation est plus délicate : il faut la faire descendre jusqu'aux opérateurs de comparaisons (qui sont alors inversés) en utilisant les lois de De Morgan. Par exemple `if (!(x < y || y < z))` devient `if(x >= y && y >= z)`.

## Extensions

### Appels de fonctions

Les appels sont implémentés comme suggéré par l'énoncé : une arête d'appel `u -> v` devient `u -> entry -> exit -> v`. On définit pour cela une fonction récursive `iter_fct : func -> Dom.t -> Dom.t` prenant le domaine de `u` et renvoyant le domaine de `v`.

### Analyse en arrière

Cette extension est disponible avec l'option `--backward` et **n'est pas compatible avec les appels**.

Lorsqu'une assertion est déclenchée, on part du domaine de la branche fausse (faisant échouer l'assertion) sur le noeud correspondant, puis on propage le fait que l'assertion a eu lieu en arrière pour tenter de retrouver une trace ratant l'assertion.

Le principe est similaire à l'analyse en avant, sauf pour les assignements.

#### Amélioration apportée

Regardons `examples/backward/sum_var_ok.c` qui est un programme correct.

```C
void main() {
    int x = rand(1 , 10);
    int y = 3;
    int sum = x+y;
    if (sum >= 6)
    {
        sum = 0; // trap for the backward guard!
        assert(x >= 3); //@OK
    }
}
```

L'analyse en avant échoue car l'appel de guard sur `sum >= 6` ne permet pas de restreindre les valeurs de son expression assignée `x+y` qui a été oubliée.

En remontant depuis l'assertion avec un domaine initial `{x = [1, 2], y = [3, 3], sum = [0, 0]}`, lorsqu'on traite `sum := x+y`, on veut être capable d'utiliser le fait que `sum = [6, oo]` pour restreindre `x, y` : prouver que `x >= 3` et conclure que `x = bottom`.

C'est le rôle de la fonction `bwd_assign x var expr r` proposée par le sujet. On impose à la racine de l'expression le domaine de la variable, puis on utilise la même fonction `propage` utilisée pour `guard`.

On ne sait rien sur la valeur de `var` avant l'assignement. On l'affecte donc à `VDom.top` mais avant, on sauvegarde son contenu pour pouvoir l'afficher. On obtient la suite de déductions suivantes :

```
File "examples/backward/sum_var_ok.c", line 8: Assertion detected (given to backward analysis) "x(1) >= 3"
Assignment at node 6: sum can be [0, 0]
Assignment at node 4: sum can be [6, oo]
Assignment at node 3: y can be [3, 3]
Assignment at node 1: no correct value for x
File "examples/backward/sum_var_ok.c", line 8: Assertion succeeded (by backward analysis) "x(1) >= 3"
```

L'analyse en arrière prouve ici **qu'aucune valeur de x cohérente avec l'échec de l'assertion n'existe** : on a réussi à prouver que l'assertion était impossible !

Inversement pour `sum_var_ko.c`, l'analyse en arrière conclut que si `x` est dans `[3, 4]`, l'assertion peut bien avoir lieu.

```
File "examples/backward/sum_var_ko.c", line 9: Assertion detected (given to backward analysis) "x(1) >= 5"
Assignment at node 6: sum can be [0, 0]
Assignment at node 4: sum can be [6, oo]
Assignment at node 3: y can be [3, 3]
Assignment at node 1: x can be [3, 4]
File "examples/backward/sum_var_ko.c", line 9: Assertion failure "x(1) >= 5"
```

#### Faiblesse

Sur des assertions d'égalité, par exemple `if (sum == 6) assert(x == 3)`, l'analyse en arrière ne peut pas faire remonter `x != 3` sur le domaine des intervalles. On échoue donc à certifier `examples/backward/fail_eq.c`.

Si on disposait d'un domaine disjonctif, l'analyse en arrière serait partie de `x = [-oo, 2] u [4, oo]` et aurait conclu à une incohérence.
